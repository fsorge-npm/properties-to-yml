<img alt="Quality Gate Status" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=alert_status"/>

<img alt="Maintainability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=sqale_rating"/>

<img alt="Reliability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=reliability_rating"/>

<img alt="Security Rating" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=security_rating"/>

<img alt="Bugs" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=bugs"/>

<img alt="Vulnerabilities" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=vulnerabilities"/>

<img alt="Coverage" src="https://sonarcloud.io/api/project_badges/measure?project=properties-to-yml&metric=coverage"/>

<img alt="Pipeline" src="https://img.shields.io/gitlab/pipeline/fsorge-npm/properties-to-yml/main"/>

# PropertiesToYml

Converts strings/files from the `properties` format to strings/files in the `yaml` format with a nice Fluent interface.


## Quick start

1. Add PropertiesToYml as a dependency of your project. It's as simple as:
```bash
npm i properties-to-yml
```

2. Import the library
```bash
import { PropertiesToYml } from 'properties-to-yml';
```

3. Start converting


## Examples

### String to string

```javascript
const output = new PropertiesToYml()
    .fromString('server.port = 8080')
    .convert()
    .toString();
```

### File to string

```javascript
const output = new PropertiesToYml()
    .fromFile('/path/to/file.properties')
    .convert()
    .toString();
```

### File to file

```javascript
new PropertiesToYml()
    .fromFile('/path/to/file.properties')
    .convert()
    .toFile('/path/to/file.yml');
```

### String to file

```javascript
new PropertiesToYml()
    .fromString('server.port = 8080')
    .convert()
    .toFile('/path/to/file.yml');
```



## White spaces

You can customize how many spaces must be used in the outputted yaml.

To do so, just call `.spaces(N)` before calling the `convert()` method.

**Example:**

```javascript
new PropertiesToYml()
    .spaces(4)
    .fromFile('/path/to/file.properties')
    .convert()
    .toFile('/path/to/file.yml');
```

  

## Develop

Write code and run tests against it with `npm test`