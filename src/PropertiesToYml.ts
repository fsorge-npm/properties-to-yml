import fs = require('fs');

/**
 * Converts a .properties string/file to .yml string/file
 * @link Examples https://www.npmjs.com/package/properties-to-yml#examples
 * @author Francesco Sorge <contact@francescosorge.com>
 */
export class PropertiesToYml {
  /**
   * Input properties string
   */
  private _s?: string;

  /**
   * Input file path
   */
  private _sourceFile?: string;

  /**
   * Output file path
   */
  private _destinationFile?: string;

  /**
   * Output YML string
   */
  private _outputString?: string;

  /**
   * Internal structure (JSON)
   */
  private readonly _internalStructure: any = {};

  /**
   * Number of spaces that the outputted YML must have for each indentation
   */
  private _spaces = 2;

  /**
   * Sets converter input from string
   * @param s string in properties format
   * @returns current instance
   */
  public fromString(s: string): PropertiesToYml {
    this._s = s;
    this._sourceFile = undefined;
    return this;
  }

  /**
   * Sets converter input from a file path (file content will be read automatically)
   * @param path file .properties apth
   * @returns current instance
   */
  public fromFile(path: string): PropertiesToYml {
    this._s = undefined;
    this._sourceFile = path;
    return this;
  }

  /**
   * Converts properties input to yml output
   * @returns current instance
   * @throws `Error` when input is empty (you didn't call `fromString(string)` or `fromFile(path)` methods)
   */
  public convert(): PropertiesToYml {
    // If input is a file
    if (this._sourceFile) {
      // Read it and save its content to the string
      this._s = fs.readFileSync(this._sourceFile, 'utf8');
    }

    if (!this._s) {
      throw new Error('Input of conversion is empty! Did you call any of fromString(string) or fromFile(path)?');
    }

    const rows = this._s.split(/\r?\n/); // Splits new lines
    for (const row of rows) {
      // If line is empty
      if (row.trim().length === 0) {
        // Skip it
        continue;
      }

      const [key, value] = row.split('=').map((part) => part.trim()); // Splits KEY=VALUE and removes white spaces
      const keys = key.split('.'); // Divides key into array (eg: spring.datasource.url becomes ['spring', 'datasource', 'url'])

      let v: any = value;
      // Tries to convert the value to a boolean
      if (v.toLowerCase() === 'true' || v.toLowerCase() === 'false') {
        v = v.toLowerCase() === 'true';
      } else {
        // If it was not a boolean, then it tries to convert it to a float/int
        const tryFloat = parseFloat(v);
        v = !isNaN(tryFloat) ? tryFloat : v; // if it was not a float/int, then it's a string
      }

      this._buildInternalStructure(this._internalStructure, keys, v);
    }

    this._outputString = this._buildOutputFromInternalStructure(this._internalStructure);
    return this;
  }

  /**
   * Recursively builds internal structure (JSON)
   * @param internalStructure JSON tree or sub-tree
   * @param keys remaining property key (each dot in a property marks a key)
   * @param value property value
   */
  private _buildInternalStructure(internalStructure: any, keys: string[], value: any): void {
    // If it's the last key (base case)
    if (keys.length === 1) {
      // Simply write the value in the data structure
      internalStructure[keys[0]] = value;
      return;
    }

    // If the key does not exist in the data structure
    if (!internalStructure[keys[0]]) {
      // Create one with an empty object
      internalStructure[keys[0]] = {};
    }

    /**
     * Recursively build internal structure by going deeper in the key-path, removing the
     * first key of the array, which is the one we just processed and pass on the value
     */
    this._buildInternalStructure(internalStructure[keys[0]], keys.slice(1), value);
  }

  /**
   * Recursively converts internal structure to valid YML string
   * @param internalStructure JSON tree or sub-tree
   * @param depth keeps track of how far inside the YML we are going
   * @returns converted YML string
   */
  private _buildOutputFromInternalStructure(internalStructure: any, depth = 0): string {
    // Starts with an empty string
    let s = ``;

    // For each key,value pair in the internal structure
    for (const [key, value] of Object.entries(internalStructure)) {
      // Write down the key with correct spaces based on the depth
      s += `${' '.repeat(this._spaces * depth)}${key}:`;

      // If the value is an object
      if (value !== null && typeof value === 'object') {
        // Recursively build it, adding one depth
        s += `\n${this._buildOutputFromInternalStructure(value, depth + 1)}`;
      } else {
        // Otherwise it's a primitive type/string
        // By default values do not have a delimiter
        let delimiter = '';
        // If the value is a string
        if (typeof value === 'string') {
          // Then we need the " delimiter
          delimiter = '"';
        }
        // Write the value
        s += ` ${delimiter}${value}${delimiter}\n`;
      }
    }
    return s;
  }

  /**
   * Writes converted YML to file
   * @param path output yml file path
   * @returns current instance
   * @throws `Error` when conversion is empty (you didn't call `convert()` method)
   */
  public toFile(path: string): PropertiesToYml {
    if (!this._outputString) {
      throw new Error('Output of conversion is empty! Did you call convert() method?');
    }

    this._destinationFile = path;

    // Write file to disk
    fs.writeFileSync(path, this._outputString);

    return this;
  }

  /**
   * Gets converted YML in string
   * @returns string in YML format
   * @throws `Error` when conversion is empty (you didn't call `convert()` method)
   */
  public toString(): string {
    if (!this._outputString) {
      throw new Error('Output of conversion is empty! Did you call convert() method?');
    }

    return this._outputString;
  }

  /**
   * Changes the number of spaces of the outputted YML
   * @param spaces Number of spaces that the outputted YML must have for each indentation
   * @returns current instance
   */
  public spaces(spaces: number): PropertiesToYml {
    this._spaces = spaces;
    return this;
  }

  /**
   * Gets number of spaces being used during conversion
   */
  public get getSpaces(): number {
    return this._spaces;
  }

  /**
   * Gets input properties string
   */
  public get inputString(): string | undefined {
    return this._s;
  }

  /**
   * Gets input file path
   */
  public get sourceFile(): string | undefined {
    return this._sourceFile;
  }

  /**
   * Gets output file path
   */
  public get destinationFile(): string | undefined {
    return this._destinationFile;
  }

  /**
   * Gets output YML string
   */
  public get outputString(): string | undefined {
    return this._outputString;
  }
}

export default PropertiesToYml;
