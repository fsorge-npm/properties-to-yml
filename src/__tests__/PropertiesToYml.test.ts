import path = require('path');
import fs = require('fs');

import { PropertiesToYml } from '../PropertiesToYml';

const currentPath = path.join(process.cwd(), 'src', '__tests__');
const spaces = 1;
const inputFile = path.join(currentPath, 'test.properties');
const outputFile = path.join(currentPath, `output-test.yml`);

test('Convert without source', () => {
  expect(() => {
    new PropertiesToYml().convert().toString();
  }).toThrowError('Input of conversion is empty! Did you call any of fromString(string) or fromFile(path)?');
});

test('Forget to call convert before toString()', () => {
  expect(() => {
    new PropertiesToYml().fromString('server.port = 8080').toString();
  }).toThrowError('Output of conversion is empty! Did you call convert() method?');
});

test('Forget to call convert before toFile()', () => {
  expect(() => {
    new PropertiesToYml().fromString('server.port = 8080').toFile(outputFile);
  }).toThrowError('Output of conversion is empty! Did you call convert() method?');
});

test('get inputString', () => {
  expect(new PropertiesToYml().fromString('server.port = 8080').inputString).toBe('server.port = 8080');
});

test('get sourceFile', () => {
  expect(new PropertiesToYml().fromFile(inputFile).sourceFile).toBe(inputFile);
});

test('get destinationFile', () => {
  expect(new PropertiesToYml().fromFile(inputFile).convert().toFile(outputFile).destinationFile).toBe(outputFile);
});

test('get outputString', () => {
  expect(new PropertiesToYml().spaces(spaces).fromString('server.port = 8080').convert().outputString).toBe(
    'server:\n port: 8080\n',
  );
});

test('get getSpaces', () => {
  expect(new PropertiesToYml().spaces(spaces).getSpaces).toBe(spaces);
});

test('From string to string', () => {
  const output = new PropertiesToYml().spaces(spaces).fromString('server.port = 8080').convert().toString();

  expect(output).toBe('server:\n port: 8080\n');
});

test('From file to string', () => {
  const output = new PropertiesToYml().spaces(spaces).fromFile(inputFile).convert().toString();

  expect(output).toBe(
    'server:\n port: 8080\nspring:\n datasource:\n  url: "jdbc:postgresql://localhost:5432/db"\n  username: "root"\n  password: "root"\n jpa:\n  properties:\n   hibernate:\n    dialect: "org.hibernate.dialect.PostgreSQLDialect"\n  show-sql: true\n',
  );
});

test('From string to file', () => {
  new PropertiesToYml().spaces(spaces).fromString('server.port = 8080').convert().toFile(outputFile);

  expect(fs.readFileSync(outputFile, 'utf8')).toBe('server:\n port: 8080\n');
});

test('From file to file', () => {
  new PropertiesToYml().spaces(spaces).fromFile(inputFile).convert().toFile(outputFile);

  expect(fs.readFileSync(outputFile, 'utf8')).toBe(
    'server:\n port: 8080\nspring:\n datasource:\n  url: "jdbc:postgresql://localhost:5432/db"\n  username: "root"\n  password: "root"\n jpa:\n  properties:\n   hibernate:\n    dialect: "org.hibernate.dialect.PostgreSQLDialect"\n  show-sql: true\n',
  );
});

test('Float parse', () => {
  const output = new PropertiesToYml().spaces(spaces).fromString('server.port = 8080').convert().toString();

  expect(output).toBe('server:\n port: 8080\n');
});
